# Field highlightjs

This module provides a preprocess function on fields. If the field has a filter
format that allows for code blocks and if the field has any of them in its value
it will automatically add the required highlightjs libraries from the CDN.
Simply install this module and your code blocks will be highlighted.

Visit /admin/config/system/field-highlightjs to change the module settings.

There you can:

* change the CSS theme
* change the library version
* change the CDN destination
* enable/disable languages

The module uses the default CDN highlightjs.min.js which supports around 40
common languages. Any languages that you enable that are not included in that
list will be added seperately.
