<?php

namespace Drupal\field_highlightjs\Form;

use Drupal\ckeditor5\Plugin\CKEditor5Plugin\CodeBlock;
use Drupal\ckeditor5\Plugin\CKEditor5PluginManagerInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\editor\Entity\Editor;
use Drupal\field_highlightjs\FieldHighlightHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Field highlightjs settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\field_highlightjs\FieldHighlightHelper service.
   *
   * @var \Drupal\field_highlightjs\FieldHighlightHelper
   */
  protected $helper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The ckeditor5 plugin manager.
   *
   * @var \Drupal\ckeditor5\Plugin\CKEditor5PluginManagerInterface
   */
  protected $ckeditor5PluginManager;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FieldHighlightHelper $helper, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, CKEditor5PluginManagerInterface $ckeditor5_plugin_manager, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    parent::__construct($config_factory);
    $this->helper = $helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->ckeditor5PluginManager = $ckeditor5_plugin_manager;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('field_highlight.helper'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('plugin.manager.ckeditor5.plugin'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'field_highlightjs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['field_highlightjs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['#attached']['library'][] = 'field_highlightjs/init';
    $form['#attached']['library'][] = 'field_highlightjs/theme';
    $form['#attached']['library'][] = 'field_highlightjs/settings';

    $form['settings'] = [
      '#type' => 'container',
    ];
    $form['settings']['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#options' => $this->config('field_highlightjs.themes')->get('themes'),
      '#default_value' => $this->config('field_highlightjs.settings')->get('theme') ?? 'default',
    ];

    $versions = $this->helper->getVersions();
    $form['settings']['version'] = [
      '#type' => 'select',
      '#title' => $this->t('Version'),
      '#options' => $versions,
      '#default_value' => $this->config('field_highlightjs.settings')->get('version') ?? reset($versions),
    ];

    $form['settings']['cdn'] = [
      '#type' => 'select',
      '#title' => $this->t('CDN'),
      '#options' => [
        'cloudflare.com' => $this->t('cloudfare.com'),
        'jsdelivr.net' => $this->t('jsdeliver.net'),
      ],
      '#default_value' => $this->config('field_highlightjs.settings')->get('cdn') ?? reset($versions),
    ];

    $languages = $this->config('field_highlightjs.languages')->get('languages');
    // Remove plaintext option. This is automatically added by ckeditor5 module.
    $languages = array_diff_key(
      $languages,
      array_flip(CodeBlock::ALWAYS_ENABLED_LANGUAGES)
    );
    $form['settings']['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages'),
      '#description' => $this->t('Select the languages you wish to support. This selection will also alter your ckeditor5 settings for codeblocks!'),
      '#options' => $languages,
      '#default_value' => $this->config('field_highlightjs.settings')->get('languages') ?? $this->helper->getCommonLanguageKeys(),
    ];

    // Attach all languages.
    foreach (array_keys($languages) as $language) {
      $form['#attached']['library'][] = 'field_highlightjs/' . $language;
    }

    $form['preview'] = [
      '#type' => 'container',
    ];
    $form['preview']['preview_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Preview language'),
      '#options' => $languages,
      '#default_value' => $form_state->getValue('preview_language') ?? 'php',
    ];
    $form['preview']['preview_markup'] = [
      '#type' => 'container',
      '#markup' => file_get_contents(__DIR__ . '/../../templates/preview.html'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration form values.
    $languages = array_filter($form_state->getValue('languages'));
    $this->config('field_highlightjs.settings')
      ->set('languages', $languages)
      ->set('theme', $form_state->getValue('theme'))
      ->set('version', $form_state->getValue('version'))
      ->set('cdn', $form_state->getValue('cdn'))
      ->save();

    // Handle ckeditor5 codeblock plugin updates.
    if (!$this->moduleHandler->moduleExists('ckeditor5')) {
      // @todo write test for this message.
      $this->messenger()->addWarning($this->t('By enabling the ckeditor5 module you can make use of the Code block plugin to add <code> html elements in your editor.'));
    }
    // @todo write test for these updates and/or messages.
    elseif ($editors = $this->entityTypeManager->getStorage('editor')->loadMultiple()) {
      foreach ($editors as $editor) {
        if (
          $editor instanceof Editor
          && $editor->getEditor() === 'ckeditor5'
          && ($settings = $editor->getSettings())
          && !empty($settings['plugins']['ckeditor5_codeBlock'])
        ) {
          $codeblock_settings = $settings['plugins']['ckeditor5_codeBlock'];
          $current_enabled_languages = $codeblock_settings['enabled_languages'] ?? [];
          // Always remove plaintext. That is added by ckeditor automatically.
          $new_enabled_languages = array_values(
            array_diff(
              array_values($languages),
              CodeBlock::ALWAYS_ENABLED_LANGUAGES
            )
          );
          if ($current_enabled_languages != $new_enabled_languages) {
            $settings['plugins']['ckeditor5_codeBlock']['enabled_languages'] = $new_enabled_languages;
            $editor->setSettings($settings);
            $editor->save();
            assert($this->ckeditor5PluginManager instanceof DefaultPluginManager);
            $this->ckeditor5PluginManager->clearCachedDefinitions();

            if ($filter_format = $editor->getFilterFormat()) {
              $this->messenger()->addWarning($this->t(
                'The enabled languages of the Code block plugin for filter format <a href="@filter-format-link">@filter-format-label</a> have been updated.',
                [
                  '@filter-format-link' => $filter_format->toUrl(
                    'edit-form',
                    ['fragment' => 'edit-editor-settings-plugins-ckeditor5-codeblock']
                  )->toString(),
                  '@filter-format-label' => $filter_format->label(),
                ],
              ));
              // Update the allowed elements string. This does not take into
              // account the class setting that is used on the available
              // languages array. We simple prefix the language id with
              // 'language-'.
              // @todo Looks like this also require a cache clear to get the
              // new options displayed in the codeblock plugin form. But that is
              // not a priority because we disable that form altogether.
              $plugin = $this->ckeditor5PluginManager->getPlugin('ckeditor5_codeBlock', $editor);
              if ($plugin instanceof ConfigurableInterface) {
                $codeblock_config = $plugin->getConfiguration();
                $filters = $filter_format->get('filters');
                if (
                  isset($filters['filter_html']['settings']['allowed_html'])
                  && isset($codeblock_config['enabled_languages'])
                ) {
                  // Add plaintext again because we want to have it in the
                  // allowed classes.
                  $languages = array_merge(['plaintext'], $codeblock_config['enabled_languages']);
                  $classes = array_map(function ($value) {
                    return 'language-' . $value;
                  }, $languages);
                  $class_string = implode(' ', $classes);
                  $filters['filter_html']['settings']['allowed_html'] = preg_replace('/<code class="(.*)">/', '<code class="' . $class_string . '">', $filters['filter_html']['settings']['allowed_html']);
                  $filter_format->set('filters', $filters);
                  $filter_format->save();
                  // Clear the filter format cache for updated allowed tags.
                  $this->cacheTagsInvalidator->invalidateTags(['config:filter.format.' . $editor->id()]);
                }
              }
            }
          }
        }
      }
    }
    // Clear the library cache.
    $this->cacheTagsInvalidator->invalidateTags(['library_info']);
    parent::submitForm($form, $form_state);
  }

}
