<?php

namespace Drupal\field_highlightjs;

use Drupal\Component\Utility\Html;
use GuzzleHttp\ClientInterface;

/**
 * Helper service FieldHighlightHelper.
 */
class FieldHighlightHelper {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a FieldHighlightHelper object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Fetches all current themes.
   *
   * @return array<string,string>
   *   The list of themes fetched from github.
   */
  public function getThemes(): array {
    // Build theme select.
    $request = $this->httpClient->request('GET', 'https://raw.githubusercontent.com/highlightjs/highlight.js/main/tools/developer.html');
    $download = $request->getBody()->getContents();
    $document = Html::load($download);
    $xpath = new \DOMXPath($document);
    $themes = [];

    if ($labels = $xpath->query('//select[@class="theme"]/option')) {
      foreach ($labels as $label) {
        if ($value = $label->nodeValue) {
          $themes[str_replace(['.css'], '', $value)] = ucwords(str_replace(
            ['.css', '-'],
            ['', ' '], $value
          ));
        }
      }
      // Removing themes that contain a dot. Some png was trashing the place.
      $themes = array_filter($themes, function ($key) {
        return strpos($key, '.') === FALSE;
      }, ARRAY_FILTER_USE_KEY);
      ksort($themes);
    }

    return $themes;
  }

  /**
   * Fetches all languages.
   *
   * @return array<string,string>
   *   The list of languages fetched from highlightjs.
   */
  public function getLanguages(): array {
    // Build languages select.
    $request = $this->httpClient->request('GET', 'https://highlightjs.org/download/');
    $download = $request->getBody()->getContents();
    $document = Html::load($download);
    $xpath = new \DOMXPath($document);
    $languages = [];

    if ($labels = $xpath->query('//form[@id="download-form"]/ul/li/label')) {
      foreach ($labels as $label) {
        if ($label instanceof \DOMElement) {
          $input = $label->getElementsByTagName('input')->item(0);
          if ($input instanceof \DOMElement) {
            $languages[str_replace('.js', '', $input->getAttribute('name'))] = trim($label->nodeValue ?? '');
          }
        }
      }
      ksort($languages);
    }

    return $languages;
  }

  /**
   * Fetches all common languages.
   *
   * @return array<string,string>
   *   The list of common languages fetched from highlightjs.
   */
  public function getCommonLanguageKeys(): array {
    // Build languages select.
    $request = $this->httpClient->request('GET', 'https://highlightjs.org/download/');
    $download = $request->getBody()->getContents();
    $document = Html::load($download);
    $xpath = new \DOMXPath($document);
    $languages = [];

    if ($labels = $xpath->query('//form[@id="download-form"]/ul[1]/li/label')) {
      foreach ($labels as $label) {
        if ($label instanceof \DOMElement) {
          $input = $label->getElementsByTagName('input')->item(0);
          if ($input instanceof \DOMElement) {
            $value = $label->nodeValue ?? '';
            $languages[str_replace('.js', '', $input->getAttribute('name'))] = trim($value);
          }
        }
      }
      ksort($languages);
    }

    return $languages;
  }

  /**
   * Fetches the versions.
   *
   * @return array<string,string>
   *   The list of versions fetched from github.
   */
  public function getVersions(): array {
    $tags = [];
    $request = $this->httpClient->request(
      'GET',
      'https://api.github.com/repos/highlightjs/highlight.js/git/refs/tags',
      ['headers' => ['Accept' => 'application/vnd.github.v3+json']]
    );
    if ($result = $request->getBody()->getContents()) {
      $versions = json_decode($result, TRUE);
      foreach ($versions as $version) {
        preg_match('~refs/tags/([\d.]+)~', $version['ref'], $matches);
        // We only keep track of versions starting from 10.
        if ($matches && is_string($matches[1]) && ((int) strtok($matches[1], '.')) >= 10) {
          $tags[$matches[1]] = $matches[1];
        }
      }
    }
    krsort($tags);

    return $tags;
  }

  /**
   * Gets the preview.html markup.
   *
   * @return array<int,string>
   *   Returns array of html markup.
   */
  public function getPreviewHtml(): array {

    // Build theme select.
    $request = $this->httpClient->request('GET', 'https://highlightjs.org/static/demo/');
    $download = $request->getBody()->getContents();
    $document = Html::load($download);
    $xpath = new \DOMXPath($document);
    $sorted_array = [];
    $sorted_markup = [];

    if ($divs = $xpath->query('//main/div')) {
      foreach ($divs as $div) {
        if ($div instanceof \DOMElement) {
          $sorted_array[] = $div;
        }
      }
      usort($sorted_array, function ($a, $b) {
        $a_code = $a->getElementsByTagName('code')->item(0);
        $b_code = $b->getElementsByTagName('code')->item(0);
        if ($a_code instanceof \DOMElement && $b_code instanceof \DOMElement) {
          return strcmp($a_code->getAttribute('class'), $b_code->getAttribute('class'));
        }
      });
      foreach ($sorted_array as $div) {
        if ($div instanceof \DOMElement
          && ($code = $div->getElementsByTagName('code')->item(0))
          && $code instanceof \DOMElement) {
          $div->removeAttribute('class');
          $div->setAttribute('class', str_replace('language-', 'hidden ', $code->getAttribute('class')));
          if (($div_owner = $div->ownerDocument)
            && $html = $div_owner->saveHTML($div)) {
            $sorted_markup[] = $html;
          }
        }
      }
    }

    return $sorted_markup;
  }

}
