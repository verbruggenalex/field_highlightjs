<?php

namespace Drupal\Tests\field_highlightjs\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\FunctionalJavascriptTests\JSWebAssert;

/**
 * Tests for Field highlightjs in the admin UI.
 *
 * @group field_highlightjs
 * @internal
 */
class AdminUiTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'ckeditor',
    'ckeditor5',
    'field_highlightjs',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'seven';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser(
      [
        'administer filters',
        'create page content',
        'edit own page content',
        'administer site configuration',
      ],
      'Testuser'
    ));
  }

  /**
   * Test that saving Field highlightjs settings displays correct message.
   */
  public function testSavingFieldHighlightjsSettingsDisplaysCorrectMessage() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    assert($assert_session instanceof JSWebAssert);

    // Switch ckeditor for full_html to ckeditor5 and take screenshot.
    $this->drupalGet('admin/config/content/formats/manage/full_html');
    if ($page->hasUncheckedField('filters[filter_html][status]')) {
      $page->checkField('filters[filter_html][status]');
      $assert_session->assertWaitOnAjaxRequest();
    }
    $page->selectFieldOption('editor[editor]', 'ckeditor5');
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save configuration');
    $this->drupalGet('admin/config/content/formats/manage/full_html');
    $assert_session->linkExists('Code Block');
    $this->clickLink('Code Block');
    $assert_session->pageTextContains('These settings are managed by Field highlightjs. Go to the Field highlightjs settings page to manage the list of enabled languages.');
    $this->takeFullSizeScreenshot('/var/www/html/tests/screenshots/textformat.png');

    // Change the Field highlightjs settings and take screenshot.
    $this->drupalGet('admin/config/system/field-highlightjs');
    $this->assertTrue($page->hasUncheckedField('languages[1c]'));
    $page->checkField('languages[1c]');
    $page->pressButton('Save configuration');
    $assert_session->pageTextContains('The configuration options have been saved.');
    $assert_session->pageTextContains('The enabled languages of the Code Block plugin for filter format Full HTML have been updated.');
    $this->takeFullSizeScreenshot('/var/www/html/tests/screenshots/settings.png');
  }

  /**
   * Takes a full size screenshot of the current page.
   *
   * @param string $absolute_path
   *   The absolute path for where to save the screenshot.
   * @param bool $error
   *   If the test failed.
   *
   * @throws \InvalidArgumentException
   * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
   * @throws \Behat\Mink\Exception\DriverException
   */
  private function takeFullSizeScreenshot(string $absolute_path, bool $error = FALSE): void {
    $update_screenshot = getenv('UPDATE_SCREENSHOTS');
    if ($update_screenshot || $error) {
      $session = $this->getSession();
      // Overreact in height to make sure there is no sidebar.
      $session->resizeWindow(1900, 4000);
      $session->executeScript('window.scrollTo(0, 0);');
      $height = $session->evaluateScript('return document.body.clientHeight;');
      $session->resizeWindow(1900, $height + 150);
      $this->createScreenshot($absolute_path);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown(): void {
    $browser_output_dir = getenv('BROWSERTEST_OUTPUT_DIRECTORY') ?? \Drupal::root() . '/sites/default/files/simpletest';
    if ($this->hasFailed() && (file_exists($browser_output_dir) || mkdir($browser_output_dir))) {
      $file_name = $browser_output_dir . '/' . $this->getName();
      $this->takeFullSizeScreenshot($file_name . '.png', TRUE);
      file_put_contents($file_name . '.html', $this->getSession()->getPage()->getContent());
    }
  }

}
